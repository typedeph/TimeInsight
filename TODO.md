[ ] Current time elapsed on current site in real time
[ ] Display current time in pretty print, i.e. Days, minutes, and seconds
[x] Url total time elapsed time value pairs. Url will be a basename
[ ] Option to dropdown base name to view time spent on different paths of site
[ ] Display a percentage breakdown of time usage for top sites list and dropdown

[ ] Create desktop server app for exporting
  [ ] CSV export
  [ ] JSON export
  [ ] Sqlite export
