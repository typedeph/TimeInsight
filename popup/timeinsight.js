let BG = browser.extension.getBackgroundPage();
let Store = BG.Store;
let timer = BG.timer;

const currentTimeHeader = (time) => `Current Session Time Spent: ${time}`;

const urlTimeCell = (topSite) => 
    `<tr>
       <td>${new URL(topSite.url).hostname}</td>
       <td>${topSite.time.reduce((accum, time) => accum + time.elapsed, 0)}</td>
     </tr>`;

const urlTimeTBody = (topSites) => `<tbody>${topSites.map(urlTimeCell).join('')}</tbody>`;

function updatePageTime(secondsElapsed) {
    const span = document.querySelector("#content p span");
    span.innerHTML = timer.getTimeElapsed();

    Store.getTopSites(2).then(function(sites) {
        console.log(sites);
        const tbl = document.querySelector("#content table");
        tbl.innerHTML = urlTimeTBody(sites);
    });
}

document.addEventListener("DOMContentLoaded", function () {
    timer.subscribe(updatePageTime);
});

window.addEventListener("unload", function () {
    timer.unsubscribe(updatePageTime);
});