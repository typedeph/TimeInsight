class _Store {
    constructor() {
        browser.storage.local.set({
            "sites": {}
        });
    }

    getSiteData(url) {
        return browser.storage.local.get("sites").then(function(store) {
            let ret = null;
            if (store["sites"].hasOwnProperty(url)) {
                ret = store["sites"][url];
            }

            return ret;
        });
    }

    sumElapsedTimes(siteData) {
        return siteData.time.reduce((accum, time) => accum + time.elapsed, 0);
    }

    updateSite(site) {
        return browser.storage.local.get("sites").then(function(store) {
            store["sites"][site.url] = site.siteData;
            browser.storage.local.set({
                "sites": store["sites"]
            });
        });
    }

    getTopSites(n) {
        return browser.storage.local.get("sites").then(function(store) {
            let sites = Object.entries(store["sites"])
                .sort(([x, a], [y, b]) => 
                  this.sumElapsedTimes(b) - this.sumElapsedTimes(a))
                .map(([url, info]) => ({
                  "url": url,
                  "time": info.time
                }));
            return sites.slice(0, n);
        }.bind(this));
    }

    clear() {
        browser.storage.local.clear();
    }
}

window.Store = new _Store();
Object.freeze(Store);