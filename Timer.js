/*
 * Accuracy of timer is within a second, since stop could be called
 * midway between a setTimeout interval
 */
class Timer {
    constructor() {
        this.timerId = 0;
        this.subscribers = [];
        this.secondsElapsed = 0;
    }

    start() {
        this.timerId = setInterval(this.notify.bind(this), 1000);
    }

    stop() {
        clearInterval(this.timerId);
    }

    getTimeElapsed() {
        return this.secondsElapsed;
    }

    resetTimeElapsed() {
        this.secondsElapsed = 0;
    }

    subscribe(cb) {
        this.subscribers.push(cb);
    }

    unsubscribe(cb) {
        let idx = this.subscribers.indexOf(cb);
        if (idx !== -1) {
            this.subscribers.splice(idx, 1);
        }
    }

    notify() {
        this.secondsElapsed++;
        this.subscribers.forEach(function(cb) { 
            cb(this.secondsElapsed) 
        }, this);
    }
}

window.Timer = Timer;