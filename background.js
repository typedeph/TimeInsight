var lastSite = {
    "url": null
};

window.secondsElapsed = 0;
window.timer = new Timer();

function tsStart(url) {
    return {
        url: url,
        startTime: new Date()
    };
}

function bootstrap(activeInfo) {
    browser.tabs.get(activeInfo.tabId).then(function (tab) {

        // Timestamp the first site.
        lastSite = tsStart(tab.url);

        // Start the timer for the current site
        timer.start();

        // Remove the bootstrap bit (everything above which runs only once).
        browser.tabs.onActivated.removeListener(bootstrap);

        browser.tabs.onActivated.addListener(function (activeInfo) {
            browser.tabs.get(activeInfo.tabId).then(function (tab) {
                updateSiteTimes(tab.url);
            });
        });

    });
}

const toUTC = (x) => Date.UTC(x.getFullYear(), x.getMonth(), x.getDay(),
    x.getHours(), x.getMinutes(), x.getSeconds(),
    x.getMilliseconds());

function datesToSecDiff(start, stop) {
    // Normalize to UTC
    let startn = toUTC(start);
    let stopn = toUTC(stop);

    return (stopn - startn) / 1000;
}

function tsStop(site) {
    return Store.getSiteData(site.url).then(function(siteStoreState) {
        let stopped = new Date();
        let siteData = {
            "started": site.startTime,
            "stopped": stopped,
            "elapsed": datesToSecDiff(site.startTime, stopped)
        };
        if (siteStoreState) {
            siteStoreState["time"].push(siteData);
            siteData = siteStoreState;
        }
        else {
            siteData = { "time": [siteData] };
        }

        Store.updateSite({
            "url": site.url,
            "siteData": siteData
        });
    });
}

function updateSiteTimes(currentUrl) {
    /**
     * Updates site times by giving the prior site an end
     * timestamp and starting the new site with a start timestamp.
     */
    tsStop(lastSite).then(function () {
        if (lastSite.url !== currentUrl) {
            timer.stop();
            timer.resetTimeElapsed();
            timer.start();
            lastSite = tsStart(currentUrl);
        }
    });
}

browser.tabs.onActivated.addListener(bootstrap);